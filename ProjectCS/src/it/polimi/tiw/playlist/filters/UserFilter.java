package it.polimi.tiw.playlist.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.polimi.tiw.playlist.enums.Role;
import it.polimi.tiw.playlist.utils.JwtUtils;

@WebFilter("/user/*")
public class UserFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		if (JwtUtils.isValid(req)) {
			if (JwtUtils.getRole(req).get().equals(Role.USER)) {
				req.setAttribute("username", JwtUtils.getUser(req).get()); // username parameter to the servlet
				chain.doFilter(request, response);
			} else {
				// Invalid Token
				resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
				resp.getWriter().println("Forbidden");
				return;
			}
		} else {
			// Invalid Token
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			resp.getWriter().println("Unhauthorized");
			return;
		}

	}

}
