package it.polimi.tiw.playlist.controllers.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.polimi.tiw.exceptions.WrongValueException;
import it.polimi.tiw.playlist.dao.PlaylistDAO;
import it.polimi.tiw.playlist.utils.CheckUtils;
import it.polimi.tiw.playlist.utils.pool.ConnectionPool;


@WebServlet("/user/do_addPlaylist")
@MultipartConfig
public class AddPlaylist extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PlaylistDAO playlistDAO = null;

	@Override
	public void init() throws ServletException {
		super.init();
		playlistDAO = new PlaylistDAO();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String name = request.getParameter("plName");
		try {
			CheckUtils.validateSimpleName(name, "playlist");		//validation
		} catch (WrongValueException e) {				//validation failed
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println(e.getMessage());
			return;
		}
		try (Connection conn = ConnectionPool.getIstance().getConnection()) {
			if (playlistDAO.createPlaylist(name, (String) request.getAttribute("username"), conn)) {
				response.setStatus(HttpServletResponse.SC_OK);
				response.getWriter().write("Succesfully added");
			} else {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.getWriter().println("You have already a playlist with this name");
				return;
			}
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("It's not possible to create the playlist now");
			return;
		}
	}

}
