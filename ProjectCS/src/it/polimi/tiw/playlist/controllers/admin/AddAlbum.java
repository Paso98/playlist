package it.polimi.tiw.playlist.controllers.admin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.Year;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import it.polimi.tiw.exceptions.WrongValueException;
import it.polimi.tiw.playlist.dao.AlbumDAO;
import it.polimi.tiw.playlist.utils.CheckUtils;
import it.polimi.tiw.playlist.utils.pool.ConnectionPool;

@WebServlet("/admin/do_addAlbum")
@MultipartConfig
public class AddAlbum extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AlbumDAO albumDAO = null;

	@Override
	public void init() throws ServletException {
		super.init();
		albumDAO = new AlbumDAO();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String title = request.getParameter("title");
		String artist = request.getParameter("artist");
		String year = request.getParameter("year");
		Part imagePart = request.getPart("cover");

		try {
			CheckUtils.validateAlbum(title, artist, year, imagePart); // validation
		} catch (WrongValueException e) { // validation failed
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println(e.getMessage());
			return;
		}
		try (Connection conn = ConnectionPool.getIstance().getConnection()) {
			Year y = Year.parse(year);
			if (albumDAO.addAlbum(title, artist, y, (String) request.getAttribute("username"), conn)) {
				/*
				 * Takes an InputStream from request parameter, takes the images folder path,
				 * asks the DB for cover name and write the .png File from the InputStream in
				 * the folder
				 */
				InputStream imageStream = imagePart.getInputStream();
				String filePath = getServletContext().getRealPath("/Images");
				File output = new File(filePath, albumDAO.getCoverPath(artist, title, conn).get());
				output.createNewFile();
				ImageIO.write(ImageIO.read(imageStream), "png", output);
				response.setStatus(HttpServletResponse.SC_OK);
				response.getWriter().write("Succesfully added");
			} else {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.getWriter().println("This Album already exists");
				return;
			}
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("It's not possible to add albums now");
		}
	}
}
