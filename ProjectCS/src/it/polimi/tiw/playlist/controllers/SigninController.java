package it.polimi.tiw.playlist.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.polimi.tiw.exceptions.WrongValueException;
import it.polimi.tiw.playlist.dao.UserDAO;
import it.polimi.tiw.playlist.utils.CheckUtils;
import it.polimi.tiw.playlist.utils.Cypher;
import it.polimi.tiw.playlist.utils.pool.ConnectionPool;

@WebServlet("/do_signin")
@MultipartConfig
public class SigninController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDAO userDAO;

	@Override
	public void init() throws ServletException {
		super.init();
		userDAO = new UserDAO();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String name = request.getParameter("username");
		String password = request.getParameter("password");
		String conf = request.getParameter("conf");
		try {
			CheckUtils.validateSimpleName(name, "Username"); // validation
			CheckUtils.validatePsw(password, conf);
		} catch (WrongValueException e) { // validation failed
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println(e.getMessage());
			return;
		}
		try (Connection conn = ConnectionPool.getIstance().getConnection()) {
			if (userDAO.signin(name, Cypher.hashSHA(password), conn)) { // signin operation
				response.setStatus(HttpServletResponse.SC_OK);
				response.getWriter().write("Succesfully registered");
			} else {
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				response.getWriter().println("Username already existing");
			}
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("It's not possible to log in now");
			return;
		}
	}
}
