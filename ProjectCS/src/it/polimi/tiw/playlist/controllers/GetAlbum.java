package it.polimi.tiw.playlist.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import it.polimi.tiw.exceptions.InvalidReferenceException;
import it.polimi.tiw.playlist.beans.Album;
import it.polimi.tiw.playlist.dao.AlbumDAO;
import it.polimi.tiw.playlist.dao.SongDAO;
import it.polimi.tiw.playlist.utils.pool.ConnectionPool;

@WebServlet("/getAlbum")
public class GetAlbum extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AlbumDAO albumDAO = null;
	private SongDAO songDAO = null;

	@Override
	public void init() throws ServletException {
		super.init();
		albumDAO = new AlbumDAO();
		songDAO = new SongDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idS = request.getParameter("id");		//album id
		int id;
		try {
			id = Integer.parseInt(idS);					//int conversion
		} catch (NumberFormatException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Invalid Album ID");
			return;
		}
		try (Connection conn = ConnectionPool.getIstance().getConnection()) {
			Album album = albumDAO.getAlbum(id, conn).orElseThrow(InvalidReferenceException::new); // get the album, if
																									// there isn't throw
																									// exception
			album.setSongs(songDAO.getAlbumSongs(id, conn));				//fill album songs
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().write(new Gson().toJson(album));

		} catch (SQLException e) {			
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("It's not possible to load this resource");
			return;
		}
		catch (InvalidReferenceException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Invalid Album ID");
			return;
		}
	}

}
