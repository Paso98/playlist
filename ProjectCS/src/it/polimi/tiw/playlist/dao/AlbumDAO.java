package it.polimi.tiw.playlist.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.mysql.cj.exceptions.MysqlErrorNumbers;

import it.polimi.tiw.playlist.beans.Album;

public class AlbumDAO {

	// queries
	private static final String GET_ALBUM_QUERY = "SELECT * FROM album WHERE album.id=?";
	private static final String GET_ALL_QUERY = "SELECT * FROM album ORDER BY year DESC";
	private static final String INSERT_QUERY = "INSERT INTO album (title, artist, year, loader) VALUES(?,?,?,?)";
	private static final String GET_COVER_PATH_QUERY = "SELECT album.cover FROM album WHERE album.title=? AND album.artist=?";

	public AlbumDAO() {
	}

	public Optional<String> getCoverPath(String artist, String title, Connection conn) throws SQLException { // A.I coverPath, we
																									// need to get that
		try (PreparedStatement statement = conn.prepareStatement(GET_COVER_PATH_QUERY)) {
			statement.setString(1, title);
			statement.setString(2, artist);
			try (ResultSet result = statement.executeQuery()) {
				String s=null;
				while (result.next()) {
					s = result.getString(1);
				}
				return Optional.ofNullable(s);
			}
		}
	}

	public Optional<Album> getAlbum(int id, Connection conn) throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(GET_ALBUM_QUERY)) {
			statement.setInt(1, id);
			try (ResultSet result = statement.executeQuery()) {
				Album a = null;
				while (result.next()) {
					a = new Album();
					a.setId(result.getInt(1)); // bean setting
					a.setTitle(result.getString(2));
					a.setArtist(result.getString(3));
					a.setYear(Year.of(result.getInt(4)));
					a.setLoader(result.getString(5));
					a.setcoverPath(result.getString(6));
				}
				return Optional.ofNullable(a);
			}
		}
	}

	public List<Album> getAllAlbums(Connection conn) throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(GET_ALL_QUERY);
				ResultSet result = statement.executeQuery()) {
			List<Album> albums = new ArrayList<Album>();

			while (result.next()) {
				Album a = new Album();
				a.setId(result.getInt(1)); // bean setting
				a.setTitle(result.getString(2));
				a.setArtist(result.getString(3));
				a.setYear(Year.of(result.getInt(4)));
				a.setLoader(result.getString(5));
				a.setcoverPath(result.getString(6));
				albums.add(a);
			}
			return albums;
		}
	}

	public boolean addAlbum(String title, String artist, Year year, String loader, Connection conn)
			throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(INSERT_QUERY)) {
			statement.setString(1, title);
			statement.setString(2, artist);
			statement.setInt(3, year.getValue());
			statement.setString(4, loader);
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			if (e.getErrorCode() == MysqlErrorNumbers.ER_DUP_ENTRY) // duplicate title + artist (UNIQUE couple)
				return false;
			else
				throw e; // dao cant handle other SQL errors
		}
	}
}
