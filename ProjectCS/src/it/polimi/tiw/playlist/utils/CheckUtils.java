package it.polimi.tiw.playlist.utils;

import java.time.LocalTime;
import java.time.Year;
import java.time.format.DateTimeParseException;

import javax.servlet.http.Part;

import it.polimi.tiw.exceptions.WrongValueException;

public class CheckUtils {

	private static final int MIN_LEN = 2;
	private static final int MAX_LEN = 30;

	private static final int MIN_PASSWORD_LEN = 4;
	private static final int MAX_PASSWORD_LEN = 18;

	// This REGEX matches only strings that contains letters and numbers, it is used
	// for playlist, username and writers
	private static final String SIMPLE_REGEX = "([\\w\\d](\\s)?)*[\\w\\d]";
	// This REGEX matches only strings that contains letters, numbers or some
	// special characters (! _ . - )
	private static final String PASSWORD_REGEX = "[a-zA-Z0-9!_\\.\\-]*";
	// This REGEX matches only strings that contains letters, numbers or some
	// special characters (! - . / ? ), it is used for song titles, artist and album
	// titles
	private static final String COMPLEX_REGEX = "([\\w\\d!\\-\\.\\/\\?](\\s)?)*[\\w\\d!\\-\\.\\/\\?]";

	private static final String YOUTUBE_ID_REGEX = "[a-zA-Z0-9_\\-]{11}";

	public static void validatePsw(String psw, String cnf_psw) throws WrongValueException {
		if (psw == null)
			throw new WrongValueException("Missing parameter password");
		if (!psw.equals(cnf_psw))
			throw new WrongValueException("Password doesn't match");
		if (psw.length() < MIN_PASSWORD_LEN)
			throw new WrongValueException("Password too short, at least " + MIN_PASSWORD_LEN);
		if (psw.length() > MAX_PASSWORD_LEN)
			throw new WrongValueException("Password too long, max " + MAX_PASSWORD_LEN);
		if (!psw.matches(PASSWORD_REGEX))
			throw new WrongValueException("Invalid characters");
	}

	public static void validateAlbum(String title, String artist, String year, Part imagePart)
			throws WrongValueException {
		validateComplexName(title, "title");
		validateComplexName(artist, "artist");
		if (imagePart == null || !(imagePart.getSubmittedFileName().endsWith(".jpg")
				|| imagePart.getSubmittedFileName().endsWith(".png")))
			throw new WrongValueException("Invalid image format");
		try {
			if (Year.parse(year).isAfter(Year.now()))
				throw new WrongValueException("Cannot insert a future Year");
		} catch (DateTimeParseException d) {
			throw new WrongValueException("Wrong year value");
		}
	}

	public static void validateSong(String title, String writer, String length, String albumS, String link)
			throws WrongValueException {
		validateComplexName(title, "title");
		validateSimpleName(writer, "writer");
		try {
			Integer.parseInt(albumS);
			if (LocalTime.parse(length).isBefore(LocalTime.of(0, 0, 30)))
				throw new WrongValueException("Song is too short");
		} catch (DateTimeParseException d) {
			throw new WrongValueException("Wrong time value");
		} catch (NumberFormatException n) {
			throw new WrongValueException("Invalid album value");
		}
		validateYoutubeId(link);

	}

	public static void validateYoutubeId(String id) throws WrongValueException {
		if (!id.matches(YOUTUBE_ID_REGEX))
			throw new WrongValueException("Invalid youtube ID");
	}

	public static void validateComplexName(String value, String paramName) throws WrongValueException {
		validateName(value, paramName, COMPLEX_REGEX);
	}

	public static void validateSimpleName(String value, String paramName) throws WrongValueException {
		validateName(value, paramName, SIMPLE_REGEX);
	}

	private static void validateName(String value, String paramName, String regex) throws WrongValueException {
		if (value == null)
			throw new WrongValueException("Missing parameter " + paramName);
		if (value.length() < MIN_LEN)
			throw new WrongValueException(paramName + " too short, at least " + MIN_LEN);
		if (value.length() > MAX_LEN)
			throw new WrongValueException(paramName + " too long, max " + MAX_LEN);
		if (!value.matches(regex))
			throw new WrongValueException("Invalid characters");
	}

}
