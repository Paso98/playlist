package it.polimi.tiw.exceptions;

/**
 * Used when there is an invalid parameter, useful to handle error messages
 */
public class WrongValueException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public WrongValueException(String message) {
		super(message);
	}

}
