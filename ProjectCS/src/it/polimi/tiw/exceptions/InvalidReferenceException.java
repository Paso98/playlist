package it.polimi.tiw.exceptions;
/**
 * Used when there is an invalid key references in DAO's queries execution, it just identifies this type of exception
 */
public class InvalidReferenceException extends Exception {
	private static final long serialVersionUID = 1L;
}
