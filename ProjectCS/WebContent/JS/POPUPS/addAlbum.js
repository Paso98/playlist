ModalAddAlbum.prototype = getModalPrototype();
function ModalAddAlbum() {
	this.target = $('#addAlbumForm');
	this.source = 'POPUPS/addAlbum.html #addAlbumFormContent';

	this.bind = function () {
		this.message = $('#errorMessageAA');
		this.messageContainer = $('#errorMessageAA').parent();
		this.submitButton = $('#addAlbumBtn');
		this.closeButton = $('#closeBtnAA');
		this.upperCloseButton = $('#closeAA');
		this.dismissButton = $('#dismissAA');
		this.form = this.submitButton.closest('form');
	}

	this.submit = function (refreshFunction) {
		var self = this;
		var token = false;

		if (typeof (Storage) !== "undefined") {
			if (localStorage.token)
				token = localStorage.token;
			if (sessionStorage.token)				//something wrong in local? Better without else
				token = sessionStorage.token;
		}
		if (!token) {								//no jwt, no way to add playlist
			self.closeForm();
			return;
		}

		if (!self.form.valid())		//client side validation
			return;

		var formData = new FormData(self.form[0]);
		$.ajax({										//ajax call
			url: 'admin/do_addAlbum',
			type: 'POST',
			data: formData,
			contentType: false,							//we need for formData
			processData: false,
			headers: { "Authorization": 'Bearer ' + token }	//jwt auth
		})
			.done((response) => {
				self.messageContainer.removeClass('alert-danger alert-success').addClass('alert-success');		//change message style
				self.message.html(response);
				if (refreshFunction) refreshFunction();
			})
			.fail((jqXHR) => {
				if (jqXHR.status === 401) {
					logout();					//remove token
					self.closeForm();
				}
				self.messageContainer.removeClass('alert-danger alert-success').addClass('alert-danger');		//change message style
				self.message.html(jqXHR.responseText);

			}).always(() => {
				self.messageContainer.show();		//show messages

			});
	}

	this.validateForm = function () {
		$.validator.addMethod("complexRegex", function (value, element) {					//custom field validator
			return this.optional(element) || value.match(/^([\w\d!\-\.\/\?](\s)?)*[\w\d!\-\.\/\?]$/);			//same regex of backend
		}, "Invalid characters, string must contains only letters, numbers, spaces and characters ! - . / ?");

		this.form.validate({ 	// initialize the plugin
			rules: {								//rules
				title: {
					required: true,
					minlength: 2,
					maxlength: 30,
					complexRegex: true,
				},
				artist: {
					required: true,
					minlength: 2,
					maxlength: 30,
					complexRegex: true,
				},
				year: {
					required: true,
					number: true,
					min: 1000,
					max: new Date().getFullYear(),
				},
				cover: {
					required: true,
					accept: "image/jpeg, image/png",
				},
			},
			messages: {								//error messages
				title: {
					required: "Please enter the album title",
					minlength: $.validator.format("Title too short, at least {0} characters"),
					maxlength: $.validator.format("Title too long, max {0} characters"),
					complexRegex: "Invalid characters, title must contains only letters, numbers, spaces and characters ! - . / ? "
				},
				artist: {
					required: "Please enter the artist",
					minlength: $.validator.format("Artist too short, at least {0} characters"),
					maxlength: $.validator.format("Artist too long, max {0} characters"),
					complexRegex: "Invalid characters, artist must contains only letters, numbers, spaces and characters ! - . / ? "
				},
				year: {
					required: "Please enter the year",
					number: "Please enter a valid year",
					min: $.validator.format("Please enter a valid year, after {0}"),
					max: $.validator.format("Please enter a valid year, before {0}"),
				},
				cover: {
					required: "Please enter the cover",
					accept: $.validator.format("Please enter a valid format (png or jpg)")
				}
	
			},
			errorPlacement: (error, element) => {	//where to place errors
				error.addClass('alert alert-danger');
				error.insertAfter(element);
			},
			wrapper: 'div',
		});
	}
}
