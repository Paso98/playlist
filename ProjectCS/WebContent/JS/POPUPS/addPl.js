ModalAddPl.prototype = getModalPrototype();
function ModalAddPl() {
	this.target = $('#addPlForm');
	this.source = 'POPUPS/addPl.html #addPlFormContent';

	this.bind = function () {
		this.message = $('#errorMessageA');
		this.messageContainer = $('#errorMessageA').parent();
		this.submitButton = $('#createBtn');
		this.closeButton = $('#closeABtn');
		this.upperCloseButton = $('#closeA');
		this.dismissButton = $('#dismissA');
		this.form = this.submitButton.closest('form');
	}

	this.submit = function (refreshFunction) {
		var self = this;
		var token = false;

		if (typeof (Storage) !== "undefined") {
			if (localStorage.token)
				token = localStorage.token;
			if (sessionStorage.token)				//something wrong in local? Better without else
				token = sessionStorage.token;
		}
		if (!token) {								//no jwt, no way to add playlist
			self.closeForm();
			return;
		}

		if (!self.form.valid())		//client side validation
			return;

		var formData = new FormData(self.form[0]);
		$.ajax({										//ajax call
			url: 'user/do_addPlaylist',
			type: 'POST',
			data: formData,
			contentType: false,							//we need for formData
			processData: false,
			headers: { "Authorization": 'Bearer ' + token }	//jwt auth
		})
			.done((response) => {
				self.messageContainer.removeClass('alert-danger alert-success').addClass('alert-success');		//change message style
				self.message.html(response);
				if (refreshFunction) refreshFunction();
			})
			.fail((jqXHR) => {
				if (jqXHR.status === 401) {
					logout();					//remove token
					self.closeForm();
				}
				self.messageContainer.removeClass('alert-danger alert-success').addClass('alert-danger');		//change message style
				self.message.html(jqXHR.responseText);

			}).always(() => {
				self.messageContainer.show();		//show messages

			});
	}

	this.validateForm = function () {
		$.validator.addMethod("playlistRegex", function (value, element) {					//custom playlist validator
			return this.optional(element) || value.match(/^([\w\d](\s)?)*[\w\d]$/);			//same regex of backend
		}, "Invalid characters, playlist must contains only letters and numbers and spaces");

		this.form.validate({ 	// initialize the plugin
			rules: {								//rules
				plName: {
					required: true,
					minlength: 2,
					maxlength: 30,
					playlistRegex: true,
				}
			},
			messages: {								//error messages
				plName: {
					required: "Please enter playlist name",
					minlength: $.validator.format("Playlist name too short, at least {0} characters"),
					maxlength: $.validator.format("Playlist name too long, max {0} characters"),
				}
			},
			errorPlacement: (error, element) => {	//where to place errors
				error.addClass('alert alert-danger');
				error.insertAfter(element);
			},
			wrapper: 'div',
		});
	}
}
