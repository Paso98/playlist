ModalLogin.prototype = getModalPrototype();
function ModalLogin(loggedRefresh) {
	this.target = $('#loginForm');
	this.source = 'POPUPS/login.html #loginFormContent';
	this.loggedRefresh=loggedRefresh;

	this.bind = function () {
		this.message = $('#errorMessage');
		this.messageContainer = $('#errorMessage').parent();
		this.submitButton = $('#loginBtn');
		this.closeButton = $('#closeBtn');
		this.upperCloseButton = $('#close');
		this.dismissButton = $('#dismiss');
		this.form = this.submitButton.closest('form');
	}

	this.submit = function () {
		var self = this;
		if (!self.form.valid())		//client side validation
			return;
		var formData = new FormData(self.form[0]);
		$.ajax({										//ajax call
			url: 'do_login',
			type: 'POST',
			data: formData,
			contentType: false,							//we need for formData
			processData: false,
		}).done((response) => {
			if (typeof (Storage) !== "undefined") {
				if (formData.get('remember'))
					localStorage.token = response;
				else
					sessionStorage.token = response;
				updateLoggedView(response, self.loggedRefresh);					//update the views
				self.closeForm();												//close the form triggering
			}
			else
				self.message.html('Please update your browser');			//cant handle storage
		}).fail((jqXHR) => {
			if (jqXHR.status === 400 || jqXHR.status === 401 || jqXHR.status === 500)	//error messages
				self.message.html(jqXHR.responseText);
			else
				self.message.html("Unexpected error");
			self.messageContainer.show();
		});
	}

	this.validateForm = function () {
		this.form.validate({ 	// initialize the plugin
			rules: {								//rules
				username: {
					required: true,
				},
				password: {
					required: true,
				}
			},
			messages: {								//error messages
				username: "Please enter your username",
				password: "Please enter your password"
			},
			errorPlacement: (error, element) => {	//where to place errors
				error.addClass('alert alert-danger');
				error.insertAfter(element);
			},
			wrapper: 'div'
		});
	}
}
