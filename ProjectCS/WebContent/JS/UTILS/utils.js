function getModalPrototype() {
	return {
		init: function (refreshFunction) {
			var self = this;
			this.target.load(self.source, function () {
				self.bind();
				self.messageContainer.hide();

				self.submitButton.click(() =>
					self.submit(refreshFunction));			//bind
				self.closeButton.click(() =>
					self.closeForm());
				self.dismissButton.click(() => {
					self.messageContainer.hide()
				});
				self.upperCloseButton.click(() => {
					self.message.html("");				//clean forms and messages
					self.form.trigger('reset');
					$('.alert').hide();
				})

				self.validateForm();				//initialize rules
			});
		},
		closeForm: function () {							//close the form with closeBtn	
			this.upperCloseButton.trigger('click');
		}
	}
}

/*
    These scripts are handling youtube API,
*/

// 2. This code loads the IFrame Player API code asynchronously.
function loadScript(url, callback) {
	var script = document.createElement("script")
	script.type = "text/javascript";
	if (script.readyState) {  // only required for IE <9
		script.onreadystatechange = function () {
			if (script.readyState === "loaded" || script.readyState === "complete") {
				script.onreadystatechange = null;
				callback();
			}
		};
	} else {  //Others
		script.onload = function () {
			callback();
		};
	}
	script.src = url;
	document.getElementsByTagName("head")[0].appendChild(script);
}

function shuffle(array) {                                            //shuffle utils
	var currentIndex = array.length, temporaryValue, randomIndex;
	// While there remain elements to shuffle...
	while (0 !== currentIndex) {
		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;
		// And swap it with the current element.
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}
	return array;
}

