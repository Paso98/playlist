/*
	These script are handling dual mode switching
*/
function switchTheme() {
	if (localStorage.storedTheme === 'light')
		toDark();
	else if (localStorage.storedTheme === 'dark')
		toLight();
}

function toDark() {
	$('#themeCSS').attr('href', 'CSS/bootstrapDark.css');
	$('#logo').attr('src', 'RES/polilogo-dark.png');
	$('#themeSwitcher').text('Light Mode');
	localStorage.storedTheme = "dark";
}

function toLight() {
	$('#themeCSS').attr('href', 'CSS/bootstrapLight.css');
	$('#logo').attr('src', 'RES/polilogo-light.png');
	$('#themeSwitcher').text('Dark Mode');
	localStorage.storedTheme = "light";
}

function loadTheme() {
	if (typeof (Storage) !== "undefined") {
		$('#themeSwitcher').click(switchTheme);
		if (!localStorage.storedTheme)
			toLight();
		else if (localStorage.storedTheme === 'light')
			toLight();
		else if (localStorage.storedTheme === 'dark')
			toDark();
	}
	else
		$('#themeSwitcher').click(() => {
			alert("Your browser doesn't support Dual Mode");
		});
}