function Player() {
    this.ytPlayer;
    this.queue = new Array(0);
    this.playingIndex = 0;
    this.show = function () {
        var self = this;
        loadScript("https://s.ytimg.com/yts/jsbin/www-widgetapi-vflZzDPU2/www-widgetapi.js", function () {
            // 3. This function creates an <iframe> (and YouTube player)
            //    after the API code downloads.
            var link = "";
            if (self.queue.length > 0) {
                link = self.queue[0];
            }
            self.ytPlayer = new YT.Player('player', {
                videoId: link,
                events: {												//interesting events
                    'onReady': self.onPlayerReady,
                    'onStateChange': (e) => self.onPlayerStateChange(e)
                }
            });
        });

        $('#shuffleSwitch').change(() => self.randomize());
        $(".feather-skip-back").click(() => self.prevSong());
        $(".feather-skip-forward").click(() => self.nextSong());
    }

    this.play = function (link) {
        this.ytPlayer.loadVideoById(link);
    }
    // 4. The API will call this function when the video player is ready.
    this.onPlayerReady = (event) => {
        event.target.playVideo();
    }
    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    this.onPlayerStateChange = function (event) {
        if (event.data == YT.PlayerState.ENDED) {
            this.nextSong();
        }
    }
    this.nextSong = function () {										//play next song in queue
        this.playingIndex = (this.playingIndex + 1) % this.queue.length;
        this.play(this.queue[this.playingIndex]);
    }

    this.prevSong = function () {										//play prev song in queue
        this.playingIndex = this.playingIndex === 0 ? this.queue.length - 1 : this.playingIndex - 1;
        this.play(this.queue[this.playingIndex]);
    }

    this.randomize = function () {										//randomize the queue for random playing
        var self = this;
        this.playingIndex = 0;

        if ($('#shuffleSwitch').prop("checked")) {
            shuffle(self.queue);
        }
        else {
            $('#songTable tr').each(function (index, el) {
                self.queue[index] = $(this).attr('data-link');
            })
        }
    }
}