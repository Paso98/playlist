function filterSongTable() {								//song table filters
    $('#songTable tr').each(function (index, el) {
        if ($(this).children('.title').html().toUpperCase().includes($('#titleSongFilter').val().toUpperCase()) &&		//title filter
            $(this).children('.writer').html().toUpperCase().includes($('#writerSongFilter').val().toUpperCase()) &&		//writer filter
            $(this).children('.album').html().toUpperCase().includes($('#albumSongFilter').val().toUpperCase()) &&		//album filter
            $(this).children('.length').html().includes($('#lengthSongFilter').val()) &&		//album filter
            $(this).children('.loader').html().toUpperCase().includes($('#loaderSongFilter').val().toUpperCase()))		//loader filter
            $(this).show();
        else
            $(this).hide();
    });
}

function filterOthSongTable()						//table filters
{
	$('#othsongTable tr').each(function (index, el) {
		if ($(this).children('.title').html().toUpperCase().includes($('#othtitleSongFilter').val().toUpperCase()) &&		//title filter
			$(this).children('.writer').html().toUpperCase().includes($('#othwriterSongFilter').val().toUpperCase()) &&		//writer filter
			$(this).children('.album').html().toUpperCase().includes($('#othalbumSongFilter').val().toUpperCase()) &&		//album filter
			$(this).children('.length').html().includes($('#othlengthSongFilter').val()) &&		//length filter
			$(this).children('.loader').html().toUpperCase().includes($('#othloaderSongFilter').val().toUpperCase()))		//loader filter
			$(this).show();
		else
			$(this).hide();
	});
}

function filterAlbumTable() {							//album table filter
	$('#albumTable tr').each(function (index, el) {
		if ($(this).children('.title').html().toUpperCase().includes($('#titleAlbumFilter').val().toUpperCase()) &&		//title filter
			$(this).children('.artist').html().toUpperCase().includes($('#artistAlbumFilter').val().toUpperCase()) &&	//artist filter
			$(this).children('.year').html().includes($('#yearAlbumFilter').val()) &&		//year filter
			$(this).children('.loader').html().toUpperCase().includes($('#loaderAlbumFilter').val().toUpperCase()))		//loader filter
			$(this).show();
		else
			$(this).hide();

	});
}

function filterPlaylistTable() {							//playlist table filter
	$('#playlistTable tr').each(function (index, el) {
		if ($(this).children('.name').html().toUpperCase().includes($('#namePlaylistFilter').val().toUpperCase()) &&		//title filter
			$(this).children('.creator').html().toUpperCase().includes($('#creatorPlaylistFilter').val().toUpperCase()) &&	//creator filter
			$(this).children('.likes').html() >= ($('#likesPlaylistFilter').val()))		//likes filter
			$(this).show();
		else
			$(this).hide();
	});
}