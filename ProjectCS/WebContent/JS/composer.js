(function () { // avoid variables ending up in the global scope
	var playlist;		//playlist data
	var othSongs;		//other songs data
	var ddManager; 		//drag&drop managment system
	var modalLogin, modalSignin, modalAddPl;	//modals
	var pageOrchestrator = new PageOrchestrator(); // main controller

	function Playlist() {
		this.name = $('#plName');		//get components from DOM
		this.creator = $('#plCreator');
		this.likes = $('#plLikes');
		this.table = $('#songTable');

		this.show = function () {
			var self = this;
			var url = new URLSearchParams(window.location.search);
			if (url.has('id')) {			//get playlist id
				$.ajax({					//ajax call
					url: 'getPlaylist',
					type: 'GET',
					data: { 'id': url.get('id') },
					datatype: 'json'
				})
					.done((response) => {
						if (response.length === 0)
							window.location.href = "homePage.html";		//redirect
						else
							self.update(response);
					})
					.fail(() => {										//redirect
						pageOrchestrator.backToPlaylist();								//redirect
					})
			}
			else
				window.location.href = "homePage.html";					//redirect
		}

		this.update = function (playlist) {
			var self = this;
			self.name.html(playlist.name);			//fill  Data
			self.creator.html(playlist.user);
			self.likes.html(playlist.likes);
			self.table.empty();						//songs population
			$.each(playlist.songs, (idx, obj) => {
				self.table.append(
					$('<tr>').append(
						$('<td>').text(obj.id),
						$('<td>').text(obj.title).addClass('title'),
						$('<td>').text(obj.writer).addClass('writer'),
						$('<td>').text(obj.album.title).addClass('album'),
						$('<td>').text(obj.length.minute.toString().padStart(2, '0') + ":" + obj.length.second.toString().padStart(2, '0')).addClass('length'),
						$('<td>').text(obj.loader).addClass('loader')
					))		//bind playing function
			});
		}
	}

	function OthSongs() {
		this.table = $('#othsongTable');

		this.show = function () {
			var token = false;
			var url = new URLSearchParams(window.location.search);
			var token = false;
			if (typeof (Storage) !== "undefined") {
				if (localStorage.token)
					token = localStorage.token;
				if (sessionStorage.token)				//something wrong in local? Better without else
					token = sessionStorage.token;
			}

			if (!token) {
				pageOrchestrator.backToPlaylist();						//no token, go back to playlist
				return;
			}
			if (url.has('id')) {						//get playlist id
				$.ajax({								//async call, we need other song we can add to playlist
					url: 'user/getOthSongs',
					type: 'GET',
					data: { 'id': url.get('id') },
					datatype: 'json',
					headers: { "Authorization": 'Bearer ' + token }		//jwt auth
				})
					.done((response) => {
						this.update(response);
					})
					.fail((jqXHR) => {
						if (jqXHR.status === 401)
							logout();				//remove token
						pageOrchestrator.backToPlaylist();			//redirect
					})
			}
			else
				window.location.href = "homePage.html";		//redirect
		}

		this.update = function (list) {
			var self = this;
			this.table.empty();
			$.each(list, (idx, obj) => {					//song population
				self.table.append(
					$('<tr>').append(
						$('<td>').text(obj.id),
						$('<td>').text(obj.title).addClass('title'),
						$('<td>').text(obj.writer).addClass('writer'),
						$('<td>').text(obj.album.title).addClass('album'),
						$('<td>').text(obj.length.minute.toString().padStart(2, '0') + ":" + obj.length.second.toString().padStart(2, '0')).addClass('length'),
						$('<td>').text(obj.loader).addClass('loader')
					))
			});
		}
	}

	function DdManager() {
		this.oth = 'othsongTable';
		this.song = 'songTable';

		this.init = function () {
			var self = this;
			var oldList, newList, startPos;
			$('.connectedSortable').sortable({

				start: (event, ui) => {					//when you start dragging...
					newList = oldList = ui.item.parent().attr('id');	//sets old list (where the row is from) and the new list (where the row will be dropped)
					startPos = ui.item.index();
				},

				stop: (event, ui) => {					//when you drop..
					var endPos = ui.item.index();		//set end position (index in the new table)
					if (oldList === self.oth && newList === self.song) {		//different cases
						//insert in pl
						self.insertSong(ui.item.find('td:first').html(), endPos);
					}
					else if (oldList === self.song && newList === self.song && endPos !== startPos) {
						//sort  pl
						self.sortSong(ui.item.find('td:first').html(), startPos, endPos);
					}
					else if (oldList === self.song && newList === self.oth) {
						self.removeSong(ui.item.find('td:first').html(), startPos);
						//remove from pl
					}
				},

				change: (event, ui) => {			//when the row position changes
					if (ui.sender) newList = ui.placeholder.parent().attr('id');	//changes the new list when the row is moved beetwen connectedSortable
				},

				connectWith: ".connectedSortable",				//connect two tables

				helper: self.fixHelperModified						//bind

			}).disableSelection();
		}

		this.fixHelperModified = function (e, tr) {
			var $originals = tr.children();				//prevent row collapse when you drag&drop
			var $helper = tr.clone();
			$helper.children().each(function (index) {
				$(this).width($originals.eq(index).width())
			});
			return $helper;
		}

		this.insertSong = function (id, endPos) {
			this.updateSongs('user/do_addSongPl', id, null, endPos)
		}

		this.removeSong = function (id, startPos) {
			this.updateSongs('user/do_removeSongPl', id, startPos)
		}

		this.sortSong = function (id, startPos, endPos) {
			this.updateSongs('user/do_sortSongPl', id, startPos, endPos)
		}

		this.updateSongs = function (servletUrl, id, startPos, endPos) {
			var token = false;
			var url = new URLSearchParams(window.location.search);
			if (typeof (Storage) !== "undefined") {
				if (localStorage.token)
					token = localStorage.token;
				if (sessionStorage.token)				//something wrong in local? Better without else
					token = sessionStorage.token;
			}
			if (!token) {
				pageOrchestrator.backToPlaylist();				//redirect
				return;
			}
			if (url.has('id')) {				//get id
				$.ajax({						//async call to sort
					url: servletUrl,
					type: 'POST',
					data: {
						'id': url.get('id'),
						'songId': id,
						'startPos': startPos,
						'endPos': endPos
					},
					headers: { "Authorization": 'Bearer ' + token }		//jwt auth
				}).fail((jqXHR) => {
					if (jqXHR.status === 401)
						logout();					//remove token
					pageOrchestrator.backToPlaylist();				//redirect
				})
			}
		}
	}

	function PageOrchestrator() {
		this.start = () => {
			var self = this;
			loadTheme();

			modalLogin = new ModalLogin();			//create modals
			modalSignin = new ModalSignin();
			modalAddPl = new ModalAddPl();

			playlist = new Playlist();
			ddManager = new DdManager();
			othSongs = new OthSongs();

			modalLogin.init();				//modals init
			modalSignin.init();
			modalAddPl.init();
			ddManager.init();
			checkLogged();

			$('.filterSongBox').keyup(filterSongTable);			//need key release for .val()
			$('.filterOthSongBox').keyup(filterOthSongTable);			//need key release for .val()
			$('#logoutNav').click(() => logout(self.backToPlaylist));

		}

		this.refresh = () => {
			playlist.show();
			othSongs.show();
		}

		this.backToPlaylist = () => {
			var url = new URLSearchParams(window.location.search);
			if (url.has('id')) {
				window.location.href = "playlist.html?id=" + url.get('id');
			}
			else
				window.location.href = "homePage.html";
		}
	}

	$(window).load(() => {							//loading function
		pageOrchestrator.start();
		pageOrchestrator.refresh();
	});
})()

