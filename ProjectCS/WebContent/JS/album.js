(function () { // avoid variables ending up in the global scope
	var album;		//album data
	var player;		//youtube player
	var modalLogin, modalSignin, modalAddAlbum, modalAddPl, modalAddSong;	//modals
	var pageOrchestrator = new PageOrchestrator(); // main controller

	function Album() {
		this.title = $('#alTitle');		//get components from DOM
		this.artist = $('#alArtist');
		this.loader = $('#alLoader');
		this.year = $('#alYear');
		this.cover = $('#alImg');
		this.table = $('#songTable');

		this.show = function () {
			var self = this;
			var url = new URLSearchParams(window.location.search);
			if (url.has('id')) {			//get album id
				$.ajax({					//ajax call
					url: 'getAlbum',
					type: 'GET',
					data: { 'id': url.get('id') },
					datatype: 'json'
				})
					.done((response) => {
						if (response.length === 0)
							window.location.href = "homePage.html";		//redirect
						else
							self.update(response);

						feather.replace();								//youtube icon
					})
					.fail(() => {										//redirect
						window.location.href = "homePage.html";
					})
			}
			else
				window.location.href = "homePage.html";					//redirect
		}

		this.update = function (album) {
			var self = this;
			self.title.html(album.title);			//fill  Data
			self.artist.html(album.artist);
			self.loader.html(album.loader);
			self.year.html(album.year.year);
			self.cover.attr("src", "Images/" + album.coverPath);
			self.table.empty();						//songs population
			$.each(album.songs, (idx, obj) => {
				player.queue[idx] = obj.link;						//queue generation
				self.table.append(
					$('<tr>').append(
						$('<td>').text(obj.id),
						$('<td>').text(obj.title).addClass('title'),
						$('<td>').text(obj.writer).addClass('writer'),
						$('<td>').text(obj.album.title).addClass('album'),
						$('<td>').text(obj.length.minute.toString().padStart(2, '0') + ":" + obj.length.second.toString().padStart(2, '0')).addClass('length'),
						$('<td>').text(obj.loader).addClass('loader'),
						$('<td>').html('<i data-feather="youtube"></i>').addClass('icon')
					).attr({
						'data-link': obj.link
					}).click(() => {								//youtube song choosing
						player.playingIndex = player.queue.indexOf(obj.link);
						player.play(obj.link)
					}))		//bind playing function
			});
		}
	}

	function PageOrchestrator() {
		this.start = () => {
			loadTheme();
			checkLogged();

			$('.filterSongBox').keyup(filterSongTable);			//need key release for .val()
			$('#logoutNav').click(() => logout());

			album = new Album();
			player = new Player();

			modalLogin = new ModalLogin();			//create modals
			modalSignin = new ModalSignin();
			modalAddAlbum = new ModalAddAlbum();
			modalAddPl = new ModalAddPl();
			modalAddSong = new ModalAddSong();

			modalLogin.init();				//modals init
			modalSignin.init();
			modalAddAlbum.init();
			modalAddPl.init();
			modalAddSong.init(() => album.show())

			feather.replace();				//youtube icon
		}

		this.refresh = () => {

			modalAddSong.loadAlbumOptions();

			album.show();
			player.show();
		}
	}

	$(window).load(() => {							//loading function
		pageOrchestrator.start();
		pageOrchestrator.refresh();
	});
})()